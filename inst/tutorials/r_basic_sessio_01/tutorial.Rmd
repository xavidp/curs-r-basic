---
title: "Llenguatge R Bàsic - Sessió 1"
tutorial:
  id: "r-basic-sessio-01"
output: 
  learnr::tutorial:
    progressive: true
    allow_skip: true
runtime: shiny_prerendered
description: "Primera sessió del curs."
---

```{r setup, include = FALSE}
library(learnr)
library(tutorial.helpers)
knitr::opts_chunk$set(echo = FALSE)
options(tutorial.exercise.timelimit = 60, 
        tutorial.storage = "local") 
```

```{r copy-code-chunk, child = system.file("child_documents/copy_button.Rmd", package = "tutorial.helpers")}
```

```{r info-section, child = system.file("child_documents/info_section.Rmd", package = "tutorial.helpers")}
```

## Curs: Llenguatge R - mòdul bàsic - (3 sessions x 5h)
###

Sessió 1 (5h)

Aquesta és la primera sessió del curs de llenguatge R - mòdul bàsic. Parts previstes:

* Introducció a R base i R modern
* Lectura i Escriptura d'arxius
* Execució condicional
* Exercici personal


## S1.1 - Introducció a R base i R modern
###


## S1.2 - Lectura i Escriptura d'arxius
###


## S1.3 - Execució condicional
###


## S1.4 - Exercici personal
###


## S1.5 - Resum
###

<!-- Two to four sentences which bring the lessons of the tutorial together for the student. What do they know now that they did not know before? How does this tutorial connect to other tutorials? OK if this is very similar to the Introduction. You made a promise as to what they would learn. You (we hope!) kept that promise.-->

```{r download-answers, child = system.file("child_documents/download_answers.Rmd", package = "tutorial.helpers")}

```
